from django.core.cache import cache
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from shortuuidfield import ShortUUIDField

from .managers import DefaultManager


class TimeStampedModel(models.Model):
    created_on = models.DateTimeField(
        _("created on"), help_text=_("Object created date and time"), default=timezone.now
    )
    modified_on = models.DateTimeField(
        _("modified on"), help_text=_("Object modified date and time"), auto_now_add=True
    )
    idx = ShortUUIDField(unique=True, db_index=True)

    class Meta:
        abstract = True

    @classmethod
    def new(cls, **kwargs):
        return cls.objects.create(**kwargs)

    def update(self, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        self.save()
        return self

    def delete(self, force_delete=True, **kwargs):
        if force_delete:
            super().delete(**kwargs)
        else:
            self.update(is_obsolete=True)
            return self


class BaseModel(TimeStampedModel):
    """
    Soft delete model
    """

    deleted_on = models.DateTimeField(
        _("deleted on"),
        null=True,
        default=None,
        blank=True,
        help_text=_("Object deleted date and time"),
    )
    is_obsolete = models.BooleanField(_("is obsolete"), default=False, help_text=_("Is the object deleted?"))
    objects = DefaultManager()

    class Meta:
        abstract = True

    def delete(self, force_delete=True, **kwargs):
        if force_delete:
            super().delete(**kwargs)
        else:
            self.update(is_obsolete=True, deleted_on=timezone.now())
            return self


class SingletonModel(TimeStampedModel):
    """
    Singleton Model
    """

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)
        self.set_cache()

    def delete(self, *args, **kwargs):
        pass

    def set_cache(self):
        cache.set(self.__class__.__name__, self)

    @classmethod
    def load(cls):
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cache.get(cls.__name__)
