# movie-api

Add local.py in settings directory.
For ease simply rename local.sample to local.py.
## How to Run [Using Docker]
```
docker-compose up --build
docker exec -it local_movie_api bash
python manage.py makemigrations
python manage.py migrate
exit
docker-compose up --build
```

## How to Run [Using Virtualenv]
```
virtualenv venv
source venv/bin/activate
pip install -r requirements/local.txt

python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```


