FROM python:3.8
LABEL maintainer="Ajay Karki <ajaykarki333@gmail.com>"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

ENV USER app
RUN useradd -ms /bin/bash ${USER}
RUN groupadd -f ${USER}

RUN mkdir -p /srv/www /var/log/movie /app/static /app/media && \
    chown -R ${USER}:${USER} /srv/www /var/log/movie /app/static /app/media

RUN apt-get update \
  && apt-get install -y build-essential curl libpq-dev vim nano --no-install-recommends
COPY . /app/
#COPY .env /app/.env
RUN python -m pip install --upgrade pip
RUN pip install -r requirements/local.txt

RUN chown -R ${USER}:${USER} .
USER ${USER}
