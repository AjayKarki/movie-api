from decouple import config
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.documentation import include_docs_urls

schema_view = get_schema_view(
    openapi.Info(
        title="Movie API",
        default_version="v1",
        description="Movie",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="admin@admin.admin"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.IsAdminUser],
)

admin.site.site_header = "Movie API Admin"
admin.site.site_title = "Movie API Admin Panel"
admin.site.index_title = "Welcome to Movie API Admin Panel"

urlpatterns = [
    path("movie-admin/", admin.site.urls),
    path("movie-swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("movie-redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path("movie-docs/", include_docs_urls(title="Movie", permission_classes=[permissions.IsAdminUser])),
    path("users/", include("users.urls")),
    path("", include("movie.urls")),
]

if config("ENVIRONMENT").lower() == "local":
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns

