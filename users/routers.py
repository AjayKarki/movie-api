from rest_framework.routers import SimpleRouter

from .views import UserAPIViewSet, UserProfileAPIViewSet

router = SimpleRouter()
router.register("profile", UserProfileAPIViewSet, basename="profile")
router.register("list", UserAPIViewSet, basename="users_list")
