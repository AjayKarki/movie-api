from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import UserAuthTokenSerializer, UserSerializer

User = get_user_model()


class UserAPIViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)
    queryset = User.objects.all()
    http_method_names = [
        "get",
        "put",
        "patch",
    ]
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_fields = [
        "is_active",
        "is_superuser",
    ]
    search_fields = ["full_name", "email"]
    lookup_field = "idx"


class UserProfileAPIViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.none()
    http_method_names = [
        "get",
        "put",
        "patch",
    ]
    lookup_field = "idx"

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

    def paginate_queryset(self, queryset):
        return None


class LoginView(APIView):
    serializer_class = UserAuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        if not user.is_active:
            return Response({"message": "You account is inactive please contact admins."}, status.HTTP_200_OK)
        try:
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)
        user_serializer = UserSerializer(user)
        return Response(
            {
                "message": "Success",
                "token": token.key,
                "user": user_serializer.data,
            },
            status.HTTP_200_OK,
        )


