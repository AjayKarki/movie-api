from django import forms
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserAdmin(UserAdmin):
    add_form = UserCreationForm
    list_display = ["id", "email", "is_active"]
    ordering = ("email",)
    fieldsets = (
        (
            "General",
            {
                "fields": (
                    "full_name",
                    "password",
                )
            },
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_superuser",
                    "is_active",
                )
            },
        ),
        ("Important Dates", {"fields": ("date_joined",)}),
    )
    add_fieldsets = (
        (
            "General",
            {
                "fields": (
                    "full_name",
                    "email",
                    "password",
                )
            },
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_superuser",
                    "is_active",
                )
            },
        ),
        ("Important Dates", {"fields": ("date_joined",)}),
    )


admin.site.register(User, UserAdmin)
