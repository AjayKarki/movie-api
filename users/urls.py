from django.urls import include, path

from .routers import router
from .views import LoginView

app_name = "users"

urlpatterns = [
    path("", include(router.urls)),
    path("login/", LoginView.as_view()),
]
