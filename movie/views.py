from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Movie, MovieComment
from .serializers import MovieSerializer, SearchMovieSerializer, MovieCommentSerializer, MovieCommentReadSerializer
from decouple import config
import humps
import requests

class MovieReadOnlyViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.filter(is_obsolete=False)
    serializer_class = MovieSerializer
    http_method_names = ["get", "post"]
    search_fields = ["title"]

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return MovieSerializer
        return SearchMovieSerializer

    def create(self, request, *args, **kwargs):
        serializer = SearchMovieSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        title = serializer.validated_data["title"]
        movie = Movie.objects.filter(title__icontains=title)
        if movie.exists():
            serializer = MovieSerializer(movie, many=True)
            return Response(
                {
                    "message": "Movie with same name exists in database.",
                    "data": serializer.data,
                }, status.HTTP_200_OK
            )
        apikey = config("MOVIE_API_KEY", default="")
        if not apikey:
            return Response({"message": "API key is not set."}, status.HTTP_400_BAD_REQUEST)
        response = requests.get(f"https://www.omdbapi.com/?apikey={apikey}&t={title}")
        if response.status_code == 200:
            data = response.json()
            data = humps.decamelize(data)
            serializer = MovieSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(
                {
                    "message": "Movie added to database.",
                    "data": serializer.data,
                }, status.HTTP_200_OK
            )
        return Response({"message": "Movie not found."}, status.HTTP_404_NOT_FOUND)



class MovieCommentViewSet(viewsets.ModelViewSet):
    queryset = MovieComment.objects.filter(is_obsolete=False)
    serializer_class = MovieCommentSerializer
    http_method_names = ["get", "post"]
    filter_fields = ["movie__title"]

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return MovieCommentReadSerializer
        return MovieCommentSerializer

    def create(self, request, *args, **kwargs):
        serializer = MovieCommentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.save(user=request.user)
        return Response({"message": "Comment added", "data": MovieCommentReadSerializer(obj).data}, status.HTTP_200_OK)