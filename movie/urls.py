from rest_framework.routers import SimpleRouter
from django.urls import path, include
from .views import MovieReadOnlyViewSet, MovieCommentViewSet

router = SimpleRouter()
router.register("movies", MovieReadOnlyViewSet, basename="movies")
router.register("comments", MovieCommentViewSet, basename="comments")

urlpatterns = [
    path("", include(router.urls)),
]