from common.serializer_mixins import BaseModelSerializer
from common.serializer_fields import DetailRelatedField

from rest_framework import serializers
from .models import Movie, MovieComment

class MovieSerializer(BaseModelSerializer):
    class Meta:
        model = Movie
        exclude = ("modified_on", "is_obsolete", "created_on", "deleted_on")


class SearchMovieSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)


class MovieCommentSerializer(BaseModelSerializer):
    class Meta:
        model = MovieComment
        exclude = ("modified_on", "created_on", "is_obsolete", "deleted_on")
        read_only_fields = ("user",)

class MovieCommentReadSerializer(BaseModelSerializer):
    movie = DetailRelatedField(model=Movie, representation='get_basic_info')
    class Meta:
        model = MovieComment
        exclude = ("modified_on", "created_on", "is_obsolete", "deleted_on")
        read_only_fields = ("user",)