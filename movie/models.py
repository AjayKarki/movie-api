from django.db import models
from common.models import BaseModel

class Movie(BaseModel):
    """
    Blank True in most fields since we are not getting data from api
    """
    title = models.CharField(max_length=255)
    year = models.IntegerField(blank=True)
    rated = models.CharField(max_length=255, blank=True)
    released = models.CharField(max_length=255, blank=True)
    runtime = models.CharField(max_length=255, blank=True)
    genre = models.CharField(max_length=255, blank=True)
    director = models.CharField(max_length=255, blank=True)
    writer = models.CharField(max_length=255, blank=True)
    actors = models.CharField(max_length=255, blank=True)
    plot = models.CharField(max_length=255, blank=True)
    language = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    awards = models.CharField(max_length=255, blank=True)
    poster = models.URLField(blank=True)
    ratings = models.JSONField(default=list, blank=True)
    metascore = models.CharField(max_length=255, blank=True)
    imdb_rating = models.FloatField(blank=True)
    imdb_votes = models.CharField(max_length=255, blank=True)
    imdb_id = models.CharField(max_length=255, blank=True)
    dvd = models.CharField(max_length=255, blank=True)
    box_office = models.CharField(max_length=255, blank=True)
    production = models.CharField(max_length=255, blank=True)
    website = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.title
    
    def get_basic_info(self):
        return {
            'id': self.id,
            'idx': self.idx,
            'title': self.title,
            'year': self.year
        }

class MovieComment(BaseModel):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='comments')
    comment = models.TextField()

    def __str__(self):
        return self.comment