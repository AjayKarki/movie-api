from django.contrib import admin
from .models import Movie, MovieComment

@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ("title", "year", "is_obsolete")
    list_filter = ("is_obsolete",)

@admin.register(MovieComment)
class MovieCommentAdmin(admin.ModelAdmin):
    list_display = ("movie", "user", "comment")
    list_filter = ("user", "movie")